package com.manoj.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aws")
public class TestResource {

	@GetMapping
	public String getMessage() {
		return "message";
	}
}
